#include "conexao.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>

//Libraries to calculate the connection time
#include <time.h>
#include <sys/time.h>

//First connection configurations
#define PORT 80
#define FIRST_IP "150.164.9.28"

//Configuration variables
#define USERNAME "grupoEtest"
#define PASSWORD "b1695d9757"


int main(int argc, char *argv[]) {

    unsigned short port = PORT;
    char ipAddress[50] = FIRST_IP;
    char serverName[50];
    char username[] = USERNAME, password[] = PASSWORD;

    //Socket's file descriptior
    int sockfd = 0, sockfd2 = 0;

    //server's address
    struct sockaddr_in servAddress, servAddress2;

    //Initialize the socket and servAddress to realize the first connection
    initializeAndConnect(&sockfd, &servAddress, ipAddress, port);

    //Calculate time connection
    struct timeval tim;
    gettimeofday(&tim, NULL);
    double t1 = tim.tv_sec + (tim.tv_usec/1000000.0);

    //Realizes the authentication to the main server
    authenticate(sockfd, username, password);

    //Gets the type of the message returned by the server
    char answer = receiveMessage(sockfd);

    //Stils wait for the server until an Error Message or an OK message
    while( answer != 'E' && answer != 'O') {
            printf("code received: %c\n", answer);
            //C Message = Next connection will be realize through IP address
            if(answer == 'C') {
                if(sockfd2 == 0)
                    nextConnectionIP(sockfd, ipAddress, &port);
                else
                    nextConnectionIP(sockfd2, ipAddress, &port);
                printf("[C message]IP received: %s\nPort received: %hu\n\n", ipAddress, port);
                //Initialize the next connection
                initializeAndConnect(&sockfd2, &servAddress2, ipAddress, port);
                authenticate(sockfd2, username, password);
            }
            //N Message = Next connection will be realize through Server Name
            else if(answer == 'N') {
                    if(sockfd2 == 0)
                        nextConnectionServerName(sockfd, ipAddress, serverName, &port);
                    else
                        nextConnectionServerName(sockfd2, ipAddress, serverName, &port);
                    printf("[N message]hostname: %s\nIP received: %s\nPorta received: %hu\n\n", serverName, ipAddress, port);
                    //Initialize the next connection
                    initializeAndConnect(&sockfd2, &servAddress2, ipAddress, port);
                    authenticate(sockfd2, username, password);

            }
            //R Message = Sever sends the password to the username used during authentication try
            else if(answer == 'R') {
                reconnection(sockfd, password);
                initializeAndConnect(&sockfd, &servAddress, ipAddress, port);
                authenticate(sockfd, username, password);
            }
            //S Message = There is no more connections to be realized
            else if(answer == 'S') {
                sendResults(sockfd, servAddress2);
                answer = receiveMessage(sockfd);
                printf("IP sent: %s | Port sent: %d\n", ipAddress, port);
                break;
            }
            answer = receiveMessage(sockfd2);
    }

    //Ok message
    if(answer == 'O') {
        printf("\nFinal message received with success\n");
    }
    //Error message
    else if(answer == 'E') {
        errorMessage(sockfd);
    }
    //Close the main connection
    close(sockfd);

    //Finish calculating the total time used by the program
    gettimeofday(&tim, NULL);
    double t2 = tim.tv_sec + (tim.tv_usec/10000000.0);
    printf("\nLabyrinth Finished. Execution time = %f seconds\n\n", (t2-t1));


    exit(EXIT_SUCCESS);
}
