#ifndef CONEXAO_H_INCLUDED
#define CONEXAO_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>

#include<netdb.h>

//Initialize socket
void initializeSocket(int *sockfd);

//Initialize Sockadd_in
void initializeSockadd_in(struct sockaddr_in *servAddress, char *ipAddress, unsigned short port);

//Establish a connection to the server through socket
void connectToServer(int sockfd, struct sockaddr_in *servAddress);

//Initialize the socket, sockaddr_in and establish the corresponding connection
void initializeAndConnect(int *sockfd, struct sockaddr_in *servAddress, char *ipAddress, unsigned short port);

//Sends the Authentication message to the server
void authenticate(int sockfd, char *username, char *password);

//Receives the Reconnection message form the server and saves the password received
char* reconnection(int sockfd, char *password);

//Receive the first byte of the message
char receiveMessage(int sockfd);

//Gets the port that will be used to the next connection through socket
unsigned short getsPort(int sockfd);

//Transform the content received in ipBuffer into an IPString on the format "xxx.xxx.xxx.xxx"
void processIPString(char *ipString, char *ipBuffer);

//Process a C message, getting the IP and the port number related to the next connection that the program will realize
void nextConnectionIP(int sockfd, char *ip, unsigned short *port);

//Gets the IP of the hostname received
void hostnameToIp(char * hostname , char* ip);

//Process a N message, getting the IP (through serverName) and the port number that will be used to the next connection
void nextConnectionServerName(int sockfd, char *ipAddress, char *serverName, unsigned short *port);

//Sends the last IP and port used by the program to server
void sendResults(int sockfd, struct sockaddr_in servAddress);

//Process a E message, getting the type of error occurred
void errorMessage(int sockfd);

//char receiveMessage(int sockfd);



#endif // CONEXAO_H_INCLUDED
