CC = gcc

CFLAGS = -Wall -pg -g3

all: main.c
	gcc -g main.c conexao.c -o tp1

run: 
	./tp1
clean:
	rm main.o conexao.o tp1
