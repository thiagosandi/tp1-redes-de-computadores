#include "conexao.h"



//Initialize socket
void initializeSocket(int *sockfd) {
    if(*sockfd > 0)
        close(*sockfd);
    if( (*sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("Error during socket creation");
        exit(1);
    }
}
//Initialize Sockadd_in
void initializeSockadd_in(struct sockaddr_in *servAddress, char *ipAddress, unsigned short port) {
    servAddress->sin_family = AF_INET;
    servAddress->sin_addr.s_addr = inet_addr(ipAddress);
    servAddress->sin_port = htons(port);
    bzero((servAddress->sin_zero), 8); //Fils with 0 the remaining of the structure
}
//Establish a connection to the server through socket
void connectToServer(int sockfd, struct sockaddr_in *servAddress) {
    //Connect and check if the connection to the server succeeded
    if( connect(sockfd, (struct sockaddr *)servAddress, sizeof(*servAddress)) < 0 ) {
        perror("Error during connection try");
        exit(1);
    }
}

//Initialize the socket, sockaddr_in and establish the corresponding connection
void initializeAndConnect(int *sockfd, struct sockaddr_in *servAddress, char *ipAddress, unsigned short port) {
    initializeSocket(sockfd);
    initializeSockadd_in(servAddress, ipAddress, port);
    connectToServer((*sockfd), servAddress);
}
//Sends the Authentication message to the server
void authenticate(int sockfd, char *username, char *password) {
    char message[22];
    sprintf(message, "A%s %s", username, password);
    //printf("authentication = %s\n", message);

    send(sockfd, message, 22, 0);
}
//Receives the Reconnection message form the server and saves the password received
char* reconnection(int sockfd, char *password) {
    char buffer[11];
    recv(sockfd, buffer, 11, 0);
    recv(sockfd, password, 11, 0);
    //printf("Password received: %s\n", password);
}
//Receive the first byte of the message
char receiveMessage(int sockfd) {
    char type;
    recv(sockfd, &type, 1, 0);
    //printf("Message received = |%c|\n", type);
    return type;
}
//Gets the port that will be used to the next connection through socket
unsigned short getsPort(int sockfd) {
    char port_temp[2];
    recv(sockfd, port_temp, 2, 0);

    unsigned short answer = *( (unsigned short*) port_temp );
    answer = ntohs(answer);
    //printf("answer = %hu\n", answer);

    return answer;
}

//Transform the content received in ipBuffer into an IPString on the format "xxx.xxx.xxx.xxx"
void processIPString(char *ipString, char *ipBuffer) {
    memset(ipString, '\0', sizeof(ipString));
    sprintf(ipString, "%u.%u.%u.%u", (unsigned char)ipBuffer[0], (unsigned char)ipBuffer[1], (unsigned char)ipBuffer[2], (unsigned char)ipBuffer[3]);

    //printf("ipString = %s\n", ipString);
}

//Process a C message, getting the IP and the port number related to the next connection that the program will realize
void nextConnectionIP(int sockfd, char *ip, unsigned short *port) {
    char buffer[4];
    recv(sockfd, buffer, 4, 0);

    //Process the ip server
    processIPString(ip, buffer);

    //Gets the port
    (*port) = getsPort(sockfd);
    //printf("port received = %u\n", *port);
}

//Gets the IP of the hostname received
void hostnameToIp(char * hostname , char* ip) {
    struct hostent *he;
    struct in_addr **addr_list;

    //Get the host info
    if ( (he = gethostbyname( hostname ) ) == NULL) {
        herror("gethostbyname");
        return;
    }

    addr_list = (struct in_addr **) he->h_addr_list;

    int i;
    for(i = 0; addr_list[i] != NULL; i++) {
        strcpy(ip , inet_ntoa(*addr_list[i]) );
        return;
    }
}


//Process a N message, getting the IP (through serverName) and the port number that will be used to the next connection
void nextConnectionServerName(int sockfd, char *ipAddress, char *serverName, unsigned short *port) {
    char length;
    memset(&length, '\0', 1);
    recv(sockfd, &length, 1, 0);
    //printf("Message size = %d\n", tam);

    //Gets the servername
    recv(sockfd, serverName, length, 0);
    serverName[length] = '\0';

    //Gets the IP from serverName
    hostnameToIp(serverName, ipAddress);

    //Gets the port
    *port = getsPort(sockfd);
}

//Sends the last IP and port used by the program to server
void sendResults(int sockfd, struct sockaddr_in servAddress) {
    char buffer[7];
    buffer[0] = 'D';
    memcpy( &(buffer[1]), &(servAddress.sin_addr.s_addr), sizeof(servAddress.sin_addr.s_addr));
    memcpy( &(buffer[5]), &(servAddress.sin_port), sizeof(servAddress.sin_port));
    send(sockfd, buffer, 7, 0);
}

//Process a E message, getting the type of error occurred
void errorMessage(int sockfd) {
    char buffer[21];

    memset(&buffer, '\0', sizeof(buffer) );
    recv(sockfd, buffer, 21, 0);
    printf("Error: %s\n", buffer);
}
